<?php

namespace Halo;

class hardware extends Controller
{

    function index()
    {
        $this->hardwares = \R::getAll('SELECT *, asset.id AS asset_id FROM asset WHERE type = 2');
    }

    function ajax_new_hardware()
    {
        $asset = \R::dispense('asset');
        $asset->name = $_POST['name'];
        $asset->serial_number = $_POST['serial_number'];
        $asset->type = 2;
        exit(json_encode(\R::store($asset)));
    }

    function ajax_delete_hardware()
    {
        // Delete every invoice row related to the asset
        \R::exec('DELETE FROM invoicerow WHERE asset_id = ?', [$_POST['id']]);

        // Delete asset
        \R::exec('DELETE FROM asset WHERE id = ?', [$_POST['id']]);
    }

}