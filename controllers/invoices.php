<?php

namespace Halo;


class invoices extends Controller
{

    function index()
    {
        $this->invoices = \R::getAll('SELECT *, invoice.id AS invoice_id FROM invoice
                                          LEFT JOIN invoicerow ON invoice.id = invoicerow.invoice_id
                                          LEFT JOIN asset ON asset.id = invoicerow.asset_id
                                          GROUP BY invoice.id');

        $this->assets = \R::getAll('SELECT *, asset.id AS asset_id FROM asset');
        $this->asset_types = \R::getAll('SELECT * FROM type');
    }

    function view()
    {
        $this->invoice = \R::getRow('SELECT * FROM invoice WHERE id = ?', [$this->getId()]);
        $this->assets = \R::getAll('SELECT *, asset.id AS asset_id FROM asset');
        $this->computers = \R::getAll('SELECT *, asset.id AS computer_id FROM asset WHERE type = 3');

        $this->invoice_rows = \R::getAll('SELECT *, invoicerow.id AS invoice_row_id FROM invoicerow
                                         LEFT JOIN asset ON asset.id = invoicerow.asset_id
                                         WHERE invoicerow.invoice_id = ?', [$this->getId()]);
    }

    function ajax_get_invoices()
    {
        $invoices = \R::getAll('SELECT *
                             FROM invoicerow
                               LEFT JOIN invoice ON invoice.id = invoicerow.invoice_id
                               LEFT JOIN asset ON asset.id = invoicerow.asset_id
                             WHERE invoicerow.asset_id = ?', [$_POST['id']]);
        exit(json_encode($invoices));
    }

    function ajax_get_invoice_row()
    {
        exit(json_encode(\R::getRow('SELECT * FROM invoicerow WHERE invoicerow.id = ?', [$_POST['id']])));
    }

    function ajax_create()
    {
        // Insert new invoice
        $invoice = \R::dispense('invoice');
        $invoice->issuer = $_POST['issuer'];
        $invoice->number = $_POST['number'];
        $invoice->date = $_POST['date'];
        $invoice_id = \R::store($invoice);

        // Insert invoice rows
        foreach ($_POST['invoice_rows'] as $row) {
            $invoice_row = \R::dispense('invoicerow');
            $invoice_row->invoice_id = $invoice_id;
            $invoice_row->asset_id = $row['asset_id'];
            $invoice_row->quantity = $row['quantity'];
            \R::store($invoice_row);
        }

        exit(File::upload($invoice_id));
    }

    function ajax_edit_invoice()
    {
        $invoice = \R::findOne('invoice', 'id = ? ', [$_POST['id']]);
        $invoice->number = $_POST['number'];
        $invoice->issuer = $_POST['issuer'];
        $invoice->date = $_POST['date'];
        \R::store($invoice);
    }


    function ajax_edit_invoice_row()
    {
        $invoice_row = \R::findOne('invoicerow', 'id = ? ', [$_POST['id']]);
        $invoice_row->asset_id = $_POST['asset_id'];
        $invoice_row->quantity = $_POST['quantity'];
        \R::store($invoice_row);
    }

    function ajax_add_invoice_row()
    {
        $invoice_row = \R::dispense('invoicerow');
        $invoice_row->invoice_id = $_POST['invoice_id'];
        $invoice_row->asset_id = $_POST['asset_id'];
        $invoice_row->quantity = $_POST['quantity'];
        \R::store($invoice_row);
    }

    function ajax_edit_invoice_row_computer()
    {
        if ($_POST['checked'] == 'true') {
            $computer_invoicerow = \R::dispense('computerinvoicerow');
            $computer_invoicerow->computer_id = $_POST['computer_id'];
            $computer_invoicerow->invoicerow_id = $_POST['invoicerow_id'];
            \R::store($computer_invoicerow);
        } else {
            \R::exec("DELETE FROM computerinvoicerow
                          WHERE invoicerow_id = ? AND computer_id = ?",
                [$_POST['invoicerow_id'], $_POST['computer_id']]);

        }

    }

    function download()
    {
        exit(File::download($this->getId()));
    }

    function upload()
    {
        header('location:' . BASE_URL . 'invoices/' . $this->getId());
        File::upload($this->getId());
        exit();
    }

}