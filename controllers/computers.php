<?php

namespace Halo;

class computers extends Controller
{

    function index()
    {
        $this->computers = \R::getAll('SELECT *, asset.id AS asset_id FROM asset WHERE type = 3');
    }

    function view()
    {
        $this->computer = \R::getRow('SELECT * FROM asset
                                          LEFT JOIN invoicerow ON invoicerow.asset_id = asset.id
                                          WHERE asset.id = ?', [$this->getId()]);

        $this->invoices = \R::getAll('SELECT *
                                           FROM computerinvoicerow
                                             LEFT JOIN invoicerow ON invoicerow.id = computerinvoicerow.invoicerow_id
                                             LEFT JOIN asset ON asset.id = invoicerow.asset_id
                                           WHERE computerinvoicerow.computer_id = ?
                                           GROUP BY asset_id', [$this->getId()]);
    }


    function ajax_delete_computer()
    {
        \R::exec('DELETE FROM asset WHERE id = ?', [$_POST['computer_id']]);
    }

    function ajax_update_computer()
    {
        $computer = \R::findOne('asset', 'id = ? ', [$_POST['computer_id']]);
        $computer->name = $_POST['computer_name'];
        $computer->serial_number = $_POST['serial_number'];
        \R::store($computer);
    }

    function ajax_new_computer()
    {
        $computer = \R::dispense('asset');
        $computer->name = $_POST['computer_name'];
        $computer->serial_number = isset($_POST['serial_number']) ? $_POST['serial_number'] : null;
        $computer->type = 3;
        \R::store($computer);
    }

    function ajax_get_computer()
    {
        $computer = \R::getRow('SELECT *
                             FROM asset
                             WHERE id = ?', [$_POST['computer_id']]);
        exit(json_encode($computer));
    }

}