<?php

namespace Halo;

class software extends Controller
{

    function index()
    {
        $this->softwares = \R::getAll('SELECT * FROM asset WHERE type = 1');
    }

    function ajax_new_software()
    {
        $asset = \R::dispense('asset');
        $asset->name = $_POST['name'];
        $asset->serial_number = isset($_POST['serial_number']) ? $_POST['serial_number'] : null;
        $asset->type = 1;
        exit(json_encode(\R::store($asset)));
    }

    function ajax_delete_software()
    {
        // Delete every invoice row related to the asset
        \R::exec('DELETE FROM invoicerow WHERE asset_id = ?', [$_POST['id']]);

        // Delete asset
        \R::exec('DELETE FROM asset WHERE id = ?', [$_POST['id']]);
    }

}