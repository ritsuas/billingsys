<div class="row">
    <div class="col-md-2">
        <h2><?= __('Hardware') ?></h2>
    </div>
    <div class="col-md-1">
        <input type="button" class="btn btn-success" value="<?= __('Add new hardware') ?>"
               style="margin-top: 20px;" data-toggle="modal" href="#new-hardware-modal">
    </div>
</div>
<table class="table table-hover">
    <thead>
    <tr>
        <th><?= __('Name') ?></th>
        <th><?= __('Serial number') ?></th>
        <th><?= __('View invoices') ?></th>
        <th><?= __('Delete') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($hardwares as $hardware): ?>
        <tr data-hardware_id="<?= $hardware['id'] ?>">
            <td><?= $hardware['name'] ?></td>
            <td><?= $hardware['serial_number'] ?></td>
            <td>
                <button class="btn btn-primary view-invoices-modal" data-toggle="modal" href="#view-invoices">
                    <?= __('View invoices') ?>
                </button>
            </td>
            <td>
                <button class="btn btn-danger delete-hardware"><?= __('Delete') ?></button>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<div class="modal fade" id="new-hardware-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Add hardware') ?></h4>
            </div>
            <div class="modal-body">

                <div class="row form-group">
                    <div class="col-md-3"><?= __('Name') ?></div>
                    <div class="col-md-9">
                        <input type="text" class="form-control add-hardware-name">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3"><?= __('Serial number') ?></div>
                    <div class="col-md-9">
                        <input type="text" class="form-control add-hardware-serial_number">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary add-hardware"><?= __('Add hardware') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="view-invoices">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Invoices') ?></h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><?= __('Invoice number') ?></th>
                        <th><?= __('Invoice issuer') ?></th>
                        <th><?= __('Invoice date') ?></th>
                        <th><?= __('View invoice rows') ?></th>
                    </tr>
                    </thead>
                    <tbody class="invoices-table">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    $(document).ready(function () {


        // Let the user add hardware for computers
        $('.add-hardware').on('click', function () {

            // Check that hardware has a name
            if ($('.add-hardware-name').val() != '') {
                $.post("hardware/new_hardware", {
                    name: $('.add-hardware-name').val(),
                    serial_number: $('.add-hardware-serial_number').val()
                }, function () {
                    location.reload();
                });
            } else {
                swal('<?= __('Please fill in the hardware name!') ?>')
            }

        });

        // Let the user delete the hardware and invoices related to it
        $('.delete-hardware').on('click', function () {
            var that = $(this);

            swal({
                    title: "<?= __('Are you sure?') ?>",
                    text: "<?= __('You\'re about to delete this hardware and invoices related to it.') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= __('Yes, delete it!') ?>",
                    closeOnConfirm: false
                }, function () {
                    $.post("hardware/delete_hardware", {
                        id: getID(that, 'hardware')
                    }, function () {
                        location.reload();
                    });
                }
            );
        });

        // Let the user view invoices for hardware
        $('.view-invoices-modal').on('click', function () {

            $.post("invoices/get_invoices", {
                id: getID($(this), 'hardware')
            }, function (invoices) {
                // Empty the table before appending
                $(".invoices-table").empty();

                // Append new rows
                $.each(invoices, function (index, invoice) {
                    $(".invoices-table").append(
                        '<tr>' +
                        '<td>' + invoice.number + '</td>' +
                        '<td>' + invoice.issuer + '</td>' +
                        '<td>' + invoice.date + '</td>' +
                        '<td>' +
                        '<a href="invoices/' + invoice.invoice_id + '"><button class="btn btn-primary"><?= __('View invoice') ?></button></a>' +
                        '</td>' +
                        '</tr>'
                    )
                });
            }, "json");

        });

    });

</script>