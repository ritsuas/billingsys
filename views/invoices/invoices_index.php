<div class="row">
    <div class="col-md-2">
        <h2><?= __('Invoices') ?></h2>
    </div>
    <div class="col-md-1">
        <input type="button" class="btn btn-success" value="<?= __('Add new invoice') ?>"
               style="margin-top: 20px;" data-toggle="modal" href="#new-invoice-modal">
    </div>
</div>
<table class="table table-hover">
    <thead>
    <tr>
        <th><?= __('Number') ?></th>
        <th><?= __('Issuer') ?></th>
        <th><?= __('Date') ?></th>
        <th><?= __('View invoice') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($invoices as $invoice): ?>
        <tr data-invoice_id="<?= $invoice['invoice_id'] ?>">
            <td><?= $invoice['number'] ?></td>
            <td><?= $invoice['issuer'] ?></td>
            <td><?= $invoice['date'] ?></td>
            <td>
                <a href="invoices/<?= $invoice['invoice_id'] ?>">
                    <button class="btn btn-primary">
                        <?= __('View invoice') ?>
                    </button>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<div class="modal fade" id="view-invoice-rows" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog invoice-image-modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Invoice rows') ?></h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><?= __('Asset') ?></th>
                        <th><?= __('Quantity') ?></th>
                    </tr>
                    </thead>
                    <tbody class="invoice-rows">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="new-invoice-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Add new invoice') ?></h4>
            </div>
            <div class="modal-body">
                <form id="invoice-form">

                    <div class="row form-group add-new-invoice">
                        <div class="col-md-3"><?= __('Issuer') ?></div>
                        <div class="col-md-9" style="margin-bottom: 10px">
                            <input type="text" name="issuer" class="form-control add-asset-invoice-issuer">
                        </div>

                        <div class="col-md-3"><?= __('Number') ?></div>
                        <div class="col-md-9" style="margin-bottom: 10px">
                            <input type="text" name="number" class="form-control add-asset-invoice-number">
                        </div>

                        <div class="col-md-3"><?= __('Date') ?></div>
                        <div class="col-md-9" style="margin-bottom: 10px">
                            <input type="date" name="date" class="form-control add-asset-invoice-date">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-3"><?= __('File') ?></div>
                        <div class="col-md-9">
                            <input type="file" class="btn btn-default add-invoice-file" name="invoice_upload">
                        </div>
                    </div>

                    <div class="row form-group" id="invoice-rows">

                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-success add-invoice-row"><?= __('Add invoice row') ?></button>
                <button type="button" class="btn btn-primary add-new-invoice-btn"><?= __('Add new invoice') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    $(document).ready(function () {
        var new_invoice_issuer = $('.add-asset-invoice-issuer');
        var new_invoice_number = $('.add-asset-invoice-number');
        var new_invoice_date = $('.add-asset-invoice-date');
        var new_invoice_file = $('.add-invoice-file');

        $('.add-new-invoice-btn').on('click', function () {

            var invoice_rows = [];
            var insert_invoice = true;

            $.each($('.invoice-row'), function () {

                var asset_selected = $(this).children().children().children('.add-invoice-row-asset');
                var quantity = $(this).children().children().children('.add-invoice-row-asset-quantity');

                if (asset_selected.val() != 0 && quantity.val() != '') {
                    invoice_rows.push({
                        asset_id: asset_selected.val(),
                        asset_quantity: quantity.val()
                    });
                } else {
                    swal('<?= __('Select asset and quantity for every row!') ?>');
                    insert_invoice = false;
                    return false;
                }

            });

            // Check that all fields have been filled
            if (insert_invoice && new_invoice_file.val() != '' && new_invoice_issuer.val() != '' && new_invoice_number.val() != '' && new_invoice_date.val() != '') {

                var formData = new FormData($('#invoice-form')[0]);

                $.ajax({
                    url: "invoices/create",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data === "OK") {
                            location.reload();
                        } else {
                            swal('<?= __('Error') ?> ' + data);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //if fails
                    }
                });

                return false;

            } else {
                swal('<?= __('Fill in all of the invoice details and atleast one invoice row!') ?>');
            }

        });

        var next_invoice_row_index = 0;

        $('.add-invoice-row').on('click', function () {

            $('#invoice-rows').append('' +
                '<div class="row form-group invoice-row" style="margin: 0">' +

                '<div class="row" style="margin: 0">' +

                '<div class="col-md-2">' +
                '<?= __('New asset') ?>' +
                '</div>' +
                '<div class="col-md-3">' +
                '<?= __('Asset') ?>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<?= __('Quantity') ?>' +
                '</div>' +

                '<div class="col-md-3">' +
                '' +
                '</div>' +

                '</div>' +

                '<div class="row" style="margin: 0">' +

                '<div class="col-md-2">' +
                '<button type="button" class="btn btn-default btn-sm open-new-asset-modal" data-toggle="modal" href="#new-asset-modal">' +
                '<?= __('Add new') ?>' +
                '</button>' +
                '</div>' +

                '<div class="col-md-3">' +
                '<select class="form-control add-invoice-row-asset" name="invoice_rows[' + next_invoice_row_index + '][asset_id]">' +
                '<option value="0">--</option>' +
                '<?php foreach ($assets as $asset): ?>' +
                '<option value="<?= $asset['asset_id'] ?>">' +
                '<?= $asset['name'] ?>' +
                '</option>' +
                '<?php endforeach; ?>' +
                '</select>' +
                '</div>' +

                '<div class="col-md-4">' +
                '<input type="text" class="form-control add-invoice-row-asset-quantity" name="invoice_rows[' + next_invoice_row_index + '][quantity]">' +
                '</div>' +

                '<div class="col-md-3">' +
                '<button type="button" class="btn btn-danger btn-sm remove-invoice-row">' +
                '<span class="glyphicon glyphicon-trash"></span>' +
                '</button>' +
                '</div>' +

                '</div>' +

                '</div>');

            next_invoice_row_index = next_invoice_row_index + 1;

            $('.remove-invoice-row').on('click', function () {
                $(this).parents('div.invoice-row').remove();
            });


            $('.open-new-asset-modal').on('click', function () {

                var that = $(this);

                $('.add-new-asset').on('click', function () {
                    if ($('.new-asset-name').val() != '') {

                        if ($('#new-asset-type').val() == 1) {
                            $.post("software/new_software", {
                                name: $('.new-asset-name').val(),
                                serial_number: $('.new-asset-serial-number').val()
                            }, function (asset_id) {
                                that.parents().next().children('.add-invoice-row-asset').append($('<option>', {
                                    value: asset_id,
                                    text: $('.new-asset-name').val()
                                }));
                                that.parents().next().children('.add-invoice-row-asset').val(asset_id);
                            });
                        } else if ($('#new-asset-type').val() == 2) {

                            if ($('.new-asset-serial-number').val() != '') {
                                $.post("hardware/new_hardware", {
                                    name: $('.new-asset-name').val(),
                                    serial_number: $('.new-asset-serial-number').val()
                                }, function (asset_id) {
                                    that.parents().next().children('.add-invoice-row-asset').append($('<option>', {
                                        value: asset_id,
                                        text: $('.new-asset-name').val()
                                    }));
                                    that.parents().next().children('.add-invoice-row-asset').val(asset_id);
                                });
                            } else {
                                swal('<?= __('Please fill in the serial number!') ?>');
                            }

                        } else {
                            $.post("computers/new_computer", {
                                computer_name: $('.new-asset-name').val(),
                                serial_number: $('.new-asset-serial-number').val()
                            }, function (asset_id) {
                                that.parents().next().children('.add-invoice-row-asset').append($('<option>', {
                                    value: asset_id,
                                    text: $('.new-asset-name').val()
                                }));
                                that.parents().next().children('.add-invoice-row-asset').val(asset_id);
                            });
                        }

                    } else {
                        swal('<?= __('Please fill in the asset name!') ?>');
                    }
                });
            });

        });


    });

</script>

<div class="modal fade" id="new-asset-modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog invoice-image-modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Add new asset') ?></h4>
            </div>
            <div class="modal-body">

                <div class="row form-group">
                    <div class="col-md-3"><?= __('Name') ?></div>
                    <div class="col-md-9">
                        <input type="text" class="form-control new-asset-name">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3"><?= __('Serial number') ?></div>
                    <div class="col-md-9">
                        <input type="text" class="form-control new-asset-serial-number">
                    </div>
                </div>

                <div class="form-group">
                    <label for="new-asset-type"><?= __('Type') ?></label>
                    <select class="form-control" id="new-asset-type">
                        <?php foreach ($asset_types as $asset_type): ?>
                            <option value="<?= $asset_type['id'] ?>"><?= $asset_type['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary add-new-asset"
                        data-dismiss="modal"><?= __('Add asset') ?></button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->