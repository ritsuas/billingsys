<div class="row">
    <div class="col-md-4">
        <h2><?= __('Invoice nr. ') . $invoice['number'] . ', ' . $invoice['issuer'] . ' (' . $invoice['date'] . ')' ?></h2>
    </div>
    <div class="col-md-2">
        <input type="button" class="btn btn-default" value="<?= __('Edit invoice') ?>"
               style="margin-top: 25px;" data-toggle="modal" href="#edit-invoice-modal">
    </div>
    <div class="col-md-2">
        <input type="button" class="btn btn-default" value="<?= __('Add row') ?>"
               style="margin-top: 25px;" data-toggle="modal" href="#add-invoice-row-modal">
    </div>
    <div class="col-md-2">
        <form method="post" action="invoices/upload/<?= $this->getId() ?>" id="replace-file"
              enctype="multipart/form-data">
            <input type="file" name="invoice_upload" class="select-file" style="display: none">
            <input type="button" class="btn btn-default replace-file" style="margin-top: 25px;"
                   value="<?= __('Replace file') ?>">
        </form>
    </div>
    <div class="col-md-2">
        <a title="View order as PDF" class="btn btn-default" style="margin-top: 25px"
           href="invoices/download/<?= $this->getId() ?>" download>
            <?= __('Download file') ?>
        </a>
    </div>
</div>
<table class="table table-hover">
    <thead>
    <tr>
        <th><?= __('Asset') ?></th>
        <th><?= __('Quantity') ?></th>
        <th><?= __('Choose computers') ?></th>
        <th><?= __('Edit row') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($invoice_rows as $invoice_row): ?>
        <tr class="invoice-row" data-invoice_row_id="<?= $invoice_row['invoice_row_id'] ?>">
            <td><?= $invoice_row['name'] ?></td>
            <td class="quantity"><?= $invoice_row['quantity'] ?></td>
            <td>
                <?php if ($invoice_row['type'] != 3): ?>
                    <?php foreach ($computers as $computer): ?>

                        <label class="checkbox-inline">
                            <input type="checkbox"
                                   value="<?= $computer['id'] ?>"
                                   data-invoice_row_id="<?= $invoice_row['invoice_row_id'] ?>"
                                   class="add_or_remove_computer"
                                <?php
                                $computers_invoicerow = \R::getAll("SELECT * FROM computerinvoicerow WHERE invoicerow_id = ?", [$invoice_row['invoice_row_id']]);
                                foreach ($computers_invoicerow as $computer_invoicerow): ?>
                                    <?= $computer_invoicerow['computer_id'] == $computer['id'] ? 'checked' : '' ?>
                                <?php endforeach; ?>>
                            <?= $computer['name'] ?>
                        </label>

                    <?php endforeach; ?>
                <?php endif; ?>
            </td>
            <td><input type="button" class="btn btn-default edit-row-btn" value="<?= __('Edit row') ?>"
                       data-toggle="modal" href="#edit-invoice-row-modal"></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<div class="modal fade" id="edit-invoice-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Edit invoice nr. ') . $invoice['number'] ?></h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><?= __('Number') ?></th>
                        <th><?= __('Issuer') ?></th>
                        <th><?= __('Date') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input class="form-control edit-invoice-number" type="text"
                                   value="<?= $invoice['number'] ?>"></td>
                        <td><input class="form-control edit-invoice-issuer" type="text"
                                   value="<?= $invoice['issuer'] ?>"></td>
                        <td><input class="form-control edit-invoice-date" type="date" value="<?= $invoice['date'] ?>">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary edit-invoice"><?= __('Edit invoice') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit-invoice-row-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Edit invoice row') ?></h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><?= __('Asset') ?></th>
                        <th><?= __('Quantity') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <select class="form-control edit-invoice-row-asset">
                                <option value="0">--</option>
                                <?php foreach ($assets as $asset): ?>
                                    <option value="<?= $asset['asset_id'] ?>">
                                        <?= $asset['name'] ?>
                                    </option>
                                <?php endforeach; ?>' +
                            </select>
                        </td>
                        <td><input type="text" class="form-control edit-invoice-row-quantity"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary edit-invoice-row"><?= __('Edit row') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="add-invoice-row-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Add invoice row') ?></h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><?= __('Asset') ?></th>
                        <th><?= __('Quantity') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <select class="form-control add-invoice-row-asset">
                                <option value="0">--</option>
                                <?php foreach ($assets as $asset): ?>
                                    <option value="<?= $asset['asset_id'] ?>">
                                        <?= $asset['name'] ?>
                                    </option>
                                <?php endforeach; ?>' +
                            </select>
                        </td>
                        <td><input type="text" class="form-control add-invoice-row-quantity"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary add-invoice-row"><?= __('Add row') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    $(document).ready(function () {

        compareQuantityToChecked();

        $('.replace-file').click(function (event) {
            $('.select-file').click();
        });

        $('.select-file').change(function (click) {
            $('form#replace-file').submit();
        });

        $('.add_or_remove_computer').on('click', function () {
            $.post("invoices/edit_invoice_row_computer", {
                checked: $(this).is(':checked'),
                invoicerow_id: $(this).data('invoice_row_id'),
                computer_id: $(this).val()
            });
            compareQuantityToChecked();
        });

        $('.edit-row-btn').on('click', function () {
            $.post("invoices/get_invoice_row", {
                id: getID($(this), 'invoice_row')
            }, function (row) {
                $('.edit-invoice-row-asset').val(row.asset_id);
                $('.edit-invoice-row-quantity').val(row.quantity);
                $('.edit-invoice-row').attr('data-invoice_row_id', row.id);
            }, 'json');

        });

        $('.edit-invoice-row').on('click', function () {
            $.post("invoices/edit_invoice_row", {
                id: $('.edit-invoice-row').data('invoice_row_id'),
                asset_id: $('.edit-invoice-row-asset').val(),
                quantity: $('.edit-invoice-row-quantity').val()
            }, function () {
                location.reload();
            });
        });

        $('.add-invoice-row').on('click', function () {
            $.post("invoices/add_invoice_row", {
                invoice_id: <?= $this->params[0] ?>,
                asset_id: $('.add-invoice-row-asset').val(),
                quantity: $('.add-invoice-row-quantity').val()
            }, function () {
                location.reload();
            });
        });

        $('.edit-invoice').on('click', function () {
            $.post("invoices/edit_invoice", {
                id: <?= $this->params[0] ?>,
                number: $('.edit-invoice-number').val(),
                issuer: $('.edit-invoice-issuer').val(),
                date: $('.edit-invoice-date').val()
            }, function () {
                location.reload();
            });
        });

    });

    function compareQuantityToChecked() {
        $.each($('.invoice-row'), function () {
            var quantity = $(this).children('.quantity').html();
            var check = $(this).find('input:checked').length;

            if (quantity <= check) {
                $(this).find('input:checkbox').not(':checked').attr('disabled', true);
            } else {
                $(this).find('input:checkbox').attr('disabled', false);

            }
        });
    }

</script>