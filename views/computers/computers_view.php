<div class="row">
    <div class="col-md-5">
        <h2><?= __('Invoices for computer') . ' "' . $computer['name'] . '"' ?></h2>
    </div>
    <div class="col-md-2">
        <a href="invoices/<?= $computer['invoice_id'] ?>">
            <input style="margin-top: 25px" type="button" class="btn btn-success"
                   value="<?= __('View computer invoice') ?>">
        </a>
    </div>
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th><?= __('Name') ?></th>
        <th><?= __('Serial Number') ?></th>
        <th><?= __('Type') ?></th>
        <th><?= __('View software invoice') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($invoices as $invoice): ?>
        <tr>
            <td><?= $invoice['name'] ?></td>
            <td><?= $invoice['serial_number'] ?></td>
            <td><?= $invoice['type'] == 1 ? __('Software') : __('Hardware') ?></td>
            <td><a href="invoices/<?= $invoice['invoice_id'] ?>">
                    <input type="button" class="btn btn-success" value="<?= __('View invoice') ?>">
                </a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>