<div class="row">
    <div class="col-md-2">
        <h2><?= __('Computers') ?></h2>
    </div>
    <div class="col-md-1">
        <input type="button" class="btn btn-success" value="<?= __('Insert new computer') ?>"
               style="margin-top: 20px;" data-toggle="modal" href="#new-computer-modal">
    </div>
</div>
<table class="table table-hover">
    <thead>
    <tr>
        <th><?= __('Computer name') ?></th>
        <th><?= __('Serial number') ?></th>
        <th><?= __('Edit computer') ?></th>
        <th><?= __('Delete computer') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($computers as $computer): ?>
        <tr data-computer_id="<?= $computer['id'] ?>">
            <td><a href="computers/<?= $computer['id'] ?>"><?= $computer['name'] ?></a></td>
            <td><?= $computer['serial_number'] ?></td>
            <td>
                <button class="btn btn-primary edit-computer-modal" data-toggle="modal" href="#edit-computer">
                    <?= __('Edit') ?>
                </button>
            </td>
            <td>
                <button class="btn btn-danger delete-computer"><?= __('Delete') ?></button>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<div class="modal fade" id="edit-computer">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Edit computer') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-3"><?= __('Computer name') ?></div>
                    <div class="col-md-9"><input type="text"
                                                 class="form-control edit-computer-name"></div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3"><?= __('Serial number') ?></div>
                    <div class="col-md-9"><input type="text"
                                                 class="form-control edit-computer-serial-number"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary update-computer"><?= __('Save changes') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="new-computer-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Insert new computer') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-3"><?= __('Computer name') ?></div>
                    <div class="col-md-9"><input type="text" class="form-control new-computer-name"></div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3"><?= __('Serial number') ?></div>
                    <div class="col-md-9"><input type="text" class="form-control new-computer-serial-number"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary insert-computer"><?= __('Insert computer') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    $(document).ready(function () {

        // Let the user deletan a computer
        $('.delete-computer').on('click', function () {
            var that = $(this);
            swal({
                    title: "<?= __('Are you sure?') ?>",
                    text: "<?= __('You\'re about to delete this computer from the database.') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= __('Yes, delete it!') ?>",
                    closeOnConfirm: false
                }, function () {
                    $.post("computers/delete_computer", {
                        computer_id: getID(that, 'computer')
                    }, function () {
                        location.reload();
                    });
                }
            );
        });

        // Update computer editing modals information
        $('.edit-computer-modal').on('click', function () {
            $.post("computers/get_computer", {
                computer_id: getID($(this), 'computer')
            }, function (computer) {
                console.log(computer);
                $('.edit-computer-name').val(computer.name);
                $('.edit-computer-serial-number').val(computer.serial_number);
            }, "json");
            $('.update-computer').data('computer_id', getID($(this), 'computer'));

        });

        // Let the user update an existing computer
        $('.update-computer').on('click', function () {
            $.post("computers/update_computer", {
                computer_id: $(this).data('computer_id'),
                computer_name: $('.edit-computer-name').val(),
                serial_number: $('.edit-computer-serial-number').val()
            }, function () {
                location.reload();
            });
        });

        // Let the user insert a new computer
        $('.insert-computer').on('click', function () {
            $.post("computers/new_computer", {
                computer_name: $('.new-computer-name').val(),
                serial_number: $('.new-computer-serial-number').val()
            }, function () {
                location.reload();
            });
        });

    });

</script>