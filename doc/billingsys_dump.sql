-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: billing_system
-- ------------------------------------------------------
-- Server version	10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `computers`
--

DROP TABLE IF EXISTS computer;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `computers` (
  `id`            INT(11)      NOT NULL AUTO_INCREMENT,
  `computer_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `computers`
--

LOCK TABLES computer WRITE;
/*!40000 ALTER TABLE computer
  DISABLE KEYS */;
INSERT INTO computer VALUES (1, 'Rasmuse arvuti');
/*!40000 ALTER TABLE computer
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `computersoftwares`
--

DROP TABLE IF EXISTS invoicerow_computers;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `computersoftwares` (
  `id`          INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `computer_id` INT(10)          NOT NULL,
  `software_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `computer_softwares_computers_id_fk` (`computer_id`),
  KEY `computer_softwares_softwares_id_fk` (`software_id`),
  CONSTRAINT `computer_softwares_computers_id_fk` FOREIGN KEY (`computer_id`) REFERENCES computer (`id`),
  CONSTRAINT `computer_softwares_softwares_id_fk` FOREIGN KEY (`software_id`) REFERENCES asset (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `computersoftwares`
--

LOCK TABLES invoicerow_computers WRITE;
/*!40000 ALTER TABLE invoicerow_computers
  DISABLE KEYS */;
INSERT INTO invoicerow_computers VALUES (1, 1, 1), (5, 1, 5);
/*!40000 ALTER TABLE invoicerow_computers
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS invoice;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id`             INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice_issuer` VARCHAR(255)     NOT NULL,
  `invoice_number` INT(11)          NOT NULL,
  `invoice_date`   DATE                      DEFAULT NULL,
  `invoice_sum`    INT(11)          NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES invoice WRITE;
/*!40000 ALTER TABLE invoice
  DISABLE KEYS */;
INSERT INTO invoice
VALUES (1, 'PHPstormi arve', 2017001, '2017-04-10', 50), (4, 'Demo issuer', 2017002, '2017-05-01', 15),
  (5, 'Demo issuer', 2017003, '2010-02-05', 200);
/*!40000 ALTER TABLE invoice
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `softwareinvoices`
--

DROP TABLE IF EXISTS softwareinvoice;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softwareinvoices` (
  `id`          INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `software_id` INT(10) UNSIGNED NOT NULL,
  `invoice_id`  INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `computer_invoices_invoices_id_fk` (`invoice_id`),
  KEY `software_invoices_softwares_id_fk` (`software_id`),
  CONSTRAINT `computer_invoices_invoices_id_fk` FOREIGN KEY (`invoice_id`) REFERENCES invoice (`id`),
  CONSTRAINT `software_invoices_softwares_id_fk` FOREIGN KEY (`software_id`) REFERENCES asset (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `softwareinvoices`
--

LOCK TABLES softwareinvoice WRITE;
/*!40000 ALTER TABLE softwareinvoice
  DISABLE KEYS */;
INSERT INTO softwareinvoice VALUES (1, 1, 1), (4, 5, 4);
/*!40000 ALTER TABLE softwareinvoice
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `softwares`
--

DROP TABLE IF EXISTS asset;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softwares` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `software_name`     VARCHAR(255)     NOT NULL,
  `software_billable` TINYINT(4)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `softwares`
--

LOCK TABLES asset WRITE;
/*!40000 ALTER TABLE asset
  DISABLE KEYS */;
INSERT INTO asset VALUES (1, 'JetBrains PHPstorm', 1), (5, 'Demo software', 1);
/*!40000 ALTER TABLE asset
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id`          INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phrase`      VARCHAR(191)     NOT NULL,
  `language`    CHAR(3)          NOT NULL,
  `translation` VARCHAR(191)              DEFAULT NULL,
  `controller`  VARCHAR(15)      NOT NULL,
  `action`      VARCHAR(20)      NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `language_phrase_controller_action_index` (`language`, `phrase`, `controller`, `action`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations`
  DISABLE KEYS */;
INSERT INTO `translations` VALUES (1, 'Action', 'ee', '{untranslated}', 'global', 'global');
/*!40000 ALTER TABLE `translations`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id`        INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(25)         NOT NULL,
  `is_admin`  TINYINT(4)          NOT NULL DEFAULT '0',
  `password`  VARCHAR(191)        NOT NULL,
  `email`     VARCHAR(191)        NOT NULL,
  `deleted`   TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`user_name`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users`
  DISABLE KEYS */;
INSERT INTO `users`
VALUES (1, 'demo', 0, '$2y$10$vTje.ndUFKHyuotY99iYkO.2aHJUgOsy2x0RMXP1UmrTe6CQsKbtm', 'demo@example.com', 0);
/*!40000 ALTER TABLE `users`
  ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2017-05-07 14:08:35
