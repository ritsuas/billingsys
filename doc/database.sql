SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Table structure for table `translation`
--

CREATE TABLE `translations` (
  `translation_id` INT(10) UNSIGNED NOT NULL,
  `phrase`         VARCHAR(191)     NOT NULL,
  `language`       CHAR(3)          NOT NULL,
  `translation`    VARCHAR(191) DEFAULT NULL,
  `controller`     VARCHAR(15)      NOT NULL,
  `action`         VARCHAR(20)      NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

--
-- Dumping data for table `translation`
--

INSERT INTO `translations` (id, `phrase`, `language`, `translation`, `controller`, `action`) VALUES
  (1, ''ACTION'', ''ee'', ''{untranslated}'', ''GLOBAL'', ''GLOBAL'');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id`   INT(10) UNSIGNED    NOT NULL,
  `user_name` VARCHAR(25)         NOT NULL,
  `is_admin`  TINYINT(4)          NOT NULL DEFAULT ''0'',
  `password`  VARCHAR(191)        NOT NULL,
  `email`     VARCHAR(191)        NOT NULL,
  `deleted`   TINYINT(1) UNSIGNED NOT NULL DEFAULT ''0''
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (id, `user_name`, `is_admin`, `password`, `email`, `deleted`) VALUES
  (1, ''demo'', 0, ''$2y$10$vTje.ndUFKHyuotY99iYkO.2aHJUgOsy2x0RMXP1UmrTe6CQsKbtm'', ''demo@example.com'', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `translation`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY `language_phrase_controller_action_index` (`language`, `phrase`, `controller`, `action`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY `UNIQUE` (user_name);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `translation`
--
ALTER TABLE `translations`
  MODIFY id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 2;