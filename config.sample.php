<?php

// Load default values
require 'system/config.default.php';

// Load local customizations
$cfg['DATABASE_HOSTNAME'] = 'localhost';
$cfg['DATABASE_DATABASE'] = 'billingsys';
$cfg['DATABASE_USERNAME'] = 'root';
$cfg['DATABASE_PASSWORD'] = '';