<?php

namespace Halo;

/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 11/05/2017
 * Time: 11:07
 */
class File
{

    static function upload($invoice_id)
    {
        $maximumFileUploadSize = self::getMaximumFileUploadSize();
        $errors = array(
            0 => "There is no error, the file uploaded with success. ",
            1 => "failisuurus peab olema v&auml;iksem kui " . self::byteConvert($maximumFileUploadSize),
            2 => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form. ",
            3 => "The uploaded file was only partially uploaded",
            4 => "No file was uploaded. ",
            5 => "Missing a temporary folder. Introduced in PHP 5.0.3. ",
            6 => "Failed to write file to disk. Introduced in PHP 5.1.0. ",
            7 => "A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help. Introduced in PHP 5.2.0. ",
        );

        $f = $_FILES["invoice_upload"];

        if ($f['error'] !== UPLOAD_ERR_OK) {
            exit(__("File upload failed") . $errors[$f['error']]);
        }

        if (!$f) {
            __('Upload failed');
            return false;
        }

        $target_dir = "uploads/" . $invoice_id;
        $uploadOk = 1;

        // Check if file already exists
        if (file_exists($target_dir . $f["name"])) {
            echo $target_dir . $f["name"];
            echo __("Sorry, file already exists.");
            $uploadOk = 0;
        }

        // Check file size
        if ($f['size'] > $maximumFileUploadSize) {
            echo __("Sorry, your file is larger than") . $maximumFileUploadSize;
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            return __("Sorry, your file was not uploaded.");
            // if everything is ok, try to upload file
        } else {
            $invoice = \R::load('invoice', $invoice_id);
            $invoice->file_name = $f["name"];
            \R::store($invoice);

            if (move_uploaded_file($f["tmp_name"], $target_dir)) {
                return "OK";
            } else {
                return __("Sorry, there was an error uploading your file.");
            }
        }

    }

    static function byteConvert($bytes)
    {
        if ($bytes == 0)
            return "0.00 B";
        $s = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        $e = floor(log($bytes, 1024));
        return round($bytes / pow(1024, $e), 2) . $s[$e];
    }

    static private function convertPHPSizeToBytes($sSize)
    {
        if (is_numeric($sSize)) {
            return $sSize;
        }
        $sSuffix = substr($sSize, -1);
        $iValue = substr($sSize, 0, -1);
        switch (strtoupper($sSuffix)) {
            case 'P':
                $iValue *= 1024;
            case 'T':
                $iValue *= 1024;
            case 'G':
                $iValue *= 1024;
            case 'M':
                $iValue *= 1024;
            case 'K':
                $iValue *= 1024;
                break;
        }
        return $iValue;
    }

    static function getMaximumFileUploadSize()
    {
        return min(self::convertPHPSizeToBytes(ini_get('post_max_size')), self::convertPHPSizeToBytes(ini_get('upload_max_filesize')));
    }

    static function download($invoice_id)
    {
        // clean the output buffer
        ob_end_clean();
        $invoice = \R::load('invoice', $invoice_id);
        $file_name = realpath(__DIR__ . '/../../uploads/' . $invoice->id);
        header("Content-length: " . filesize($file_name));
        header("Content-type:" . mime_content_type($file_name));
        header("Content-Disposition: attachment; filename=$invoice->file_name");
        return file_get_contents($file_name);
    }

}