/**
 * Created by rasmu on 06/05/2017.
 */
function getID(element, name) {
    return $(element).parents('tr').data(name + '_id');
}